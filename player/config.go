package player

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

// Config structure
type Config struct {
	Version         string `json:"version"`
	RefreshInterval int    `json:"refresh_interval"`
	SmallBet        int    `json:"small_bet"`
	LargeBet        int    `json:"large_bet"`
	AllIn           bool   `json:"all_in"`
	Fold            bool   `json:"fold"`
	Random          bool   `json:"random"`
}

// NewConfig creates a new Config instance
func NewConfig() *Config {
	return &Config{
		Version:         "default config version",
		RefreshInterval: 15,
		SmallBet:        20,
		LargeBet:        50,
		AllIn:           false,
		Fold:            false,
		Random:          false}
}

// LoadConfig loads the config from Dropbox
func LoadConfig() (*Config, error) {
	log.Println("Reloading configuration")
	resp, err := http.Get("https://dl.dropboxusercontent.com/u/819938/leanpoker/config.json")

	if err != nil {
		return nil, err
	}

	configJSON, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	var config = NewConfig()
	json.Unmarshal(configJSON, config)

	return config, nil
}

// String returns string representation of config
func (c *Config) String() string {
	str, err := json.Marshal(config)

	if err != nil {
		log.Printf("Error: %s\n", err)
		return "<error>"
	}

	return string(str)
}
