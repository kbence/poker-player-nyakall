package player

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"time"

	"gitlab.com/kbence/poker-player-nyakall/leanpoker"
)

// VERSION contains version string
const VERSION = "Brave gopher"

var config = NewConfig()

// RefreshConfig updates config every once in a while
func RefreshConfig() {
	for {
		cfg, err := LoadConfig()

		if err != nil {
			log.Printf("Error updating configuration: %s\n", err)
		} else {
			config = cfg
			log.Printf("Config reloaded: %s\n", config)
		}

		time.Sleep(time.Duration(config.RefreshInterval) * time.Second)
	}
}

// BetRequest returns bet request
func BetRequest(state *leanpoker.Game) int {
	player := leanpoker.NewAdvancedPlayer(state)

	if config.Fold {
		fmt.Println("BET Folding!")
		return player.Fold()
	}

	if config.AllIn {
		fmt.Println("BET All in!")
		return player.AllIn()
	}

	if config.Random {
		switch rand.Int() % 3 {
		case 0:
			fmt.Println("RAND Folding!")
			return player.Fold()
		case 1:
			fmt.Println("RAND All in!")
			return player.AllIn()
		case 2:
			fmt.Println("RAND Continue...")
		}
	}

	minRaise := player.Raise(0)

	numberOfPlayers := 0
	for _, player := range state.Players {
		if player.Status != "out" {
			numberOfPlayers++
		}
	}
	if numberOfPlayers == 2 {
		return player.AllIn()
	}

	if leanpoker.Cards(player.Player.HoleCards).High() {
		minRaise = player.Raise(config.SmallBet)
		fmt.Println("BET high card in hand, bet: ", minRaise)
	} else if leanpoker.Cards(player.Player.HoleCards).Pair() {
		fmt.Println("BET pair in hand, bet: ", minRaise)
		minRaise = player.Raise(config.LargeBet)
	} else if leanpoker.Cards(player.Player.HoleCards).PairIn(state.CommunityCards) {
		minRaise = player.Raise(config.LargeBet)
		fmt.Println("BET pair with community cards, bet: ", minRaise)
	}

	finalBet := int(math.Min(float64(minRaise), float64(state.Players[state.InAction].Stack/2)))
	fmt.Println("BET final bet: ", finalBet)
	return finalBet
}

// Showdown is called at the end of a round
func Showdown(state *leanpoker.Game) {
	numPlayersIn := 0
	var winner = "Nobody"

	for _, player := range state.Players {
		if player.Status != "out" {
			numPlayersIn++
			winner = player.Name
		}
	}

	if numPlayersIn == 1 {
		log.Printf("Winner: %s\n", winner)
	}
}

// Version returns version string
func Version() string {
	return fmt.Sprintf("%s - [%s]", VERSION, config.Version)
}
