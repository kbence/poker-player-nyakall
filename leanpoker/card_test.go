package leanpoker

import "testing"

func TestRankNum(t *testing.T) {
	var c Card

	c.Rank = "3"
	if c.RankNum() != 3 {
		t.Error("Failed converting number", c.Rank)
	}

	c.Rank = "J"
	if c.RankNum() != 11 {
		t.Error("Failed converting number", c.Rank)
	}

	c.Rank = "A"
	if c.RankNum() != 14 {
		t.Error("Failed converting number", c.Rank)
	}

}

func TestHighSingle(t *testing.T) {
	var c Card

	c.Rank = "3"
	if c.High() {
		t.Error("Considered", c.Rank, "as high")
	}

	c.Rank = "10"
	if !c.High() {
		t.Error("Considered", c.Rank, "as low")
	}

	c.Rank = "A"
	if !c.High() {
		t.Error("Considered", c.Rank, "as low")
	}

}

func TestHighBoth(t *testing.T) {
	var cs Cards

	cs = Cards{
		Card{"9", "clubs"},
		Card{"11", "spades"},
	}
	if cs.High() {
		t.Error("Misidentified", cs, "as both high")
	}

	cs = Cards{
		Card{"10", "clubs"},
		Card{"10", "spades"},
	}
	if !cs.High() {
		t.Error("Missed minimum high cards", cs)
	}

	cs = Cards{
		Card{"10", "clubs"},
		Card{"14", "spades"},
	}
	if !cs.High() {
		t.Error("Missed minimum high cards", cs)
	}

}

func TestPair(t *testing.T) {
	var cs Cards

	cs = Cards{
		Card{"10", "clubs"},
		Card{"11", "spades"},
	}
	if cs.Pair() {
		t.Error("Misspaired", cs)
	}

	cs = Cards{
		Card{"10", "clubs"},
		Card{"10", "spades"},
	}
	if !cs.Pair() {
		t.Error("Missed pair", cs)
	}

}

func TestPairIn(t *testing.T) {

	var hand Cards
	var community Cards

	hand = Cards{
		Card{"10", "clubs"},
		Card{"11", "spades"},
	}
	community = Cards{
		Card{"10", "hearts"},
	}
	if !hand.PairIn(community) {
		t.Error("Missed pair with", hand, "in", community)
	}

	hand = Cards{
		Card{"10", "clubs"},
		Card{"10", "spades"},
	}
	community = Cards{
		Card{"11", "hearts"},
	}
	if hand.PairIn(community) {
		t.Error("Misidentified pair in own hand", hand)
	}

	hand = Cards{
		Card{"10", "clubs"},
		Card{"11", "spades"},
	}
	community = Cards{
		Card{"12", "hearts"},
	}
	if hand.PairIn(community) {
		t.Error("Misidentified pair with", hand, "in", community)
	}

	hand = Cards{
		Card{"10", "clubs"},
		Card{"11", "spades"},
	}
	community = Cards{
		Card{"12", "hearts"},
		Card{"10", "hearts"},
	}
	if !hand.PairIn(community) {
		t.Error("Missed pair with", hand, "in", community)
	}

}
