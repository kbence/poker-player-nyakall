package leanpoker

import "math"

// Player container player stuff
type Player struct {
	// Id of the player (same as the index)
	Id int `json:"id"`

	// Name specified in the tournament config
	Name string `json:"name"`

	// Status of the player:
	//   - active: the player can make bets, and win the current pot
	//   - folded: the player folded, and gave up interest in
	//       the current pot. They can return in the next round.
	//   - out: the player lost all chips, and is out of this sit'n'go
	Status string `json:"status"`

	// Version identifier returned by the player
	Version string `json:"version"`

	// Amount of chips still available for the player.
	// (Not including the chips the player bet in this round)
	Stack int `json:"stack"`

	// The amount of chips the player put into the pot
	Bet int `json:"bet"`

	// The cards of the player. This is only visible for your own player
	// except after showdown, when cards revealed are also included.
	HoleCards []Card `json:"hole_cards"`
}

// AdvancedPlayer is an advanced player
type AdvancedPlayer struct {
	Game   *Game
	Player *Player
}

// NewAdvancedPlayer creates a new instance
func NewAdvancedPlayer(game *Game) *AdvancedPlayer {
	player := AdvancedPlayer{Game: game, Player: &game.Players[game.InAction]}

	return &player
}

// Fold folds
func (p *AdvancedPlayer) Fold() int {
	return 0
}

// Check checks
func (p *AdvancedPlayer) Check() int {
	return p.Game.CurrentBuyIn - p.Player.Bet
}

// Raise raises
func (p *AdvancedPlayer) Raise(amount int) int {
	return p.Game.CurrentBuyIn - p.Player.Bet + int(math.Max(float64(amount), float64(p.Game.MinimumRaise)))
}

// AllIn gives all in
func (p *AdvancedPlayer) AllIn() int {
	return p.Player.Stack
}
