package leanpoker

import "strconv"

type Card struct {
	// Rank of the card. Possible values are numbers 2-10 and J,Q,K,A
	Rank string `json:"rank"`

	// Suit of the card. Possible values are: clubs,spades,hearts,diamonds
	Suit string `json:"suit"`
}

// Array of Card
type Cards []Card

// Returns true if the first two Card items have the same rank
func (cs Cards) Pair() bool {
	if cs[0].Rank == cs[1].Rank {
		return true
	} else {
		return false
	}
}

// Returns true if any Card in "Community Cards" (cc) has the same rank
func (cs Cards) PairIn(cc Cards) bool {
	pair := false

	for _, c := range cc {
		for _, s := range cs {
			if (Cards{c, s}).Pair() {
				pair = true
				break
			}
		}
	}

	return pair
}

// Returns true if both the first two Cards are "high cards". Only worth
// calling on your own hand
func (cs Cards) High() bool {
	if cs[0].High() && cs[1].High() {
		return true
	} else {
		return false
	}
}

// Maps card ranks to integers, e.g. "J"->11
func (c Card) RankNum() int {
	var num int
	if x, err := strconv.Atoi(c.Rank); err == nil {
		num = x
	} else {
		switch c.Rank {
		case "J":
			num = 11
		case "Q":
			num = 12
		case "K":
			num = 13
		case "A":
			num = 14
		}
	}
	return num
}

// Returns true if the cards rank is greater than a magic number
func (c Card) High() bool {
	if c.RankNum() > 9 {
		return true
	} else {
		return false
	}
}
